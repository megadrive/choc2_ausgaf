
var fs = require('fs');
var steam = require('steam');
var irc = require('irc');
var _ = require('underscore');
var settings = require('./settings');
var http = require('http');

// Greeting message. Unused?
var greetingMessage = "choc2!! I probably don't work right.";

// The games available to the bot.
// TODO: Can bot play games he doesn't own? Create an isolated test case.
var availableSteamGameIds = [
	440,  // Team Fortress 2
	34900 // Bad Rats
];

// Protocol enum
var EProtocol = {
	Steam: 0,
	IRC: 1
};

exports.choc2 = choc2;
function choc2() {
	var self = this;

	self.info = {
		version: "1.0.1 (bad_rats)",
		author: "Megadrive",
		repo: "https://bitbucket.org/megadrive/choc2"
	};

	self.settings = {};

	self.steam = undefined;
	self.irc = undefined;

	self.irc_nicks = undefined;

	self.http = new http.createServer();

	/**
	 * Initialise all variables, start the HTTP server for persistence.
	 */
	this.init = function(){
		console.log("choc2: version " + self.info.version);

		self.settings = settings.load();

		self.http.on('request', function(request, response){
			var pageContent = '';

			response.writeHead(200, {"Content-Type": "text/html"});

			pageContent = "<pre>Steam <-> IRC\n\n";
			pageContent += "Status:\n";
			pageContent += "\tSteam: " + self.steam && self.steam.loggedOn ? 'online' : 'offline' + "\n";
			pageContent += "\tIRC: probably online\n";

			response.end(pageContent);
		});

		self.http.listen(process.env.PORT || 80);
		self.http.on('error', function(e){
			console.log('server error:', e);
		});
	};

	/**
	 * Connect to protocols. This is the start point.
	 */
	this.connect = function(doListeners){
		console.log("choc2: connecting");

		doListeners = doListeners || true;

		self.joinIrc(doListeners);
		self.joinSteam(doListeners);

		if (fs.existsSync('servers')) {
			//steam.servers = JSON.parse(fs.readFileSync('servers'));
		}
		else{
			console.error('SERROR: Steam couldnt find servers file.');
			process.exit(1); // exit because only one Steam server is known to work.
		}

		console.log(" ");
	};

	/**
	 * Disconnects from the services.
	 */
	// TODO: Add this functionality. Would require removing all listeners and deleting (if you can?) self.irc and self.steam and resetting related objects.
	this.disconnect = function(){
		self.irc.disconnect();
		self.steam.logOff();
	};

	/**
	 * Reconnects to everything. Doesn't add listners, because they are not unique and will get doubled.
	 */
	this.reconnect = function(){
		self.disconnect();
		self.connect(false);
	};

	/**
	 * Join IRC and add our custom listeners.
	 */
	this.joinIrc = function(doListeners){
		console.log("       joining irc");

		self.irc = new irc.Client(self.settings.irc.server, self.settings.irc.nick, {
			'userName': 'choc2',
			'realName': 'choc2, for ausgaf <3',
			'port': 6667,
			'debug': false,
			'showErrors': false,
			'autoRejoin': true,
			'autoConnect': false,
			'channels': self.settings.irc.channels,
			'secure': false,
			'selfSigned': false,
			'certExpired': false,
			'floodProtection': false,
			'floodProtectionDelay': 1000,
			'sasl': false,
			'stripColors': false,
			'channelPrefixes': "&#",
			'messageSplit': 512
		});

		if( doListeners === true ){
			// Catch-all error listener.
			self.irc.on('error', function(e){
				console.error('IERROR: ' + e);
			});

			// If someone joins the chat, send to Steam.
			self.irc.on('join' + self.settings.irc.output_channel, function(nick){
				self.send(EProtocol.Steam, nick + ' has joined the chat.');
			});

			// If someone leaves the chat (known as a 'part'), send to Steam.
			self.irc.on('part' + self.settings.irc.output_channel, function(nick, reason){
				self.send(EProtocol.Steam, nick + ' has left. (' + reason + ').');
			});

			// See above.
			self.irc.on('quit', function(nick, reason){
				self.send(EProtocol.Steam, nick + ' has left. (' + reason + ').');
			});

			// If someone is kicked, send to Steam. Include reason and who by.
			self.irc.on('kick' + self.settings.irc.output_channel, function(nick, by, reason){
				self.send(EProtocol.Steam, nick + ' was kicked by ' + by + ' because ' + reason + '.');
			});

			// node-irc does not store the users in a channel in its object, so we much cache them ourselves.
			self.irc.on('names', function(channel, nicks){
				if( channel.toLowerCase() == self.settings.irc.output_channel.toLowerCase() ){
					self.irc_nicks = Object.keys(nicks);
				}
			});

			// If someone sends a message, AND Steam is logged on, send message to the chat.
			self.irc.on('message' + self.settings.irc.output_channel, function(nick, text){
				if( self.steam && self.steam.loggedOn ){
					self.send(EProtocol.Steam, nick + ": " + text);
				}
				self.doCommand(text);
			});

			// Connection pm, can currently be done by anyone.
			// TODO: Add admin, perhaps hard-coded?
			self.irc.on('pm', function(nick, text){
				if( text === '.reconnect' ){
					self.reconnect();
				}
			});

			// The Raw event is fired literally when anything happens on IRC. We use it to try to keep the bot to have the nickname we have specified.
			// TODO: Look into having a set limit of times it tries to change nick an hour, could get banned from servers by spamming NICK too much.
			self.irc.on('raw', function(){
				// check for nick
				if( self.irc.nick != self.settings.irc.nick ){
					self.irc.send('NICK', self.settings.irc.nick);
				}
			});
		}

		self.irc.connect();
	};

	/**
	 * Joins steam and, if applicable, add the custom listeners to the object.
	 */
	this.joinSteam = function(doListeners){
		console.log("       joining steam");

		self.steam = new steam.SteamClient();

		self.steam.logOn({
			'accountName': self.settings.steam.username,
			'password': self.settings.steam.password
		});

		if( doListeners === true ){
			/**
			 * On log on, play a random game, join the chat and set the settings we need to set, including our state (Online).
			 */
			self.steam.on('loggedOn', function(){
				console.log("       connected to Steam");

				// NOTE: Let's play games for funsies!
				self.doRandomGame();

				self.steam.setPersonaName(self.settings.steam.nick);
				self.steam.setPersonaState(steam.EPersonaState.Online);
				self.steam.joinChat(self.settings.steam.chatRoomId);
			});

			/**
			 * Possibly need to add more to this.
			 */
			self.steam.on('loggedOff', function(){
				console.log("       disconnected from Steam");
			});

			/**
			 * On Steam chat message, send to IRC and also do commands.
			 */
			self.steam.on('chatMsg', function(chatRoomId, msg, EChatEntryType, steamId){
				var nick = self.getSteamName(steamId);
				self.send(EProtocol.IRC, irc.colors.wrap('orange', nick) + ": " + msg);
				self.doCommand(msg);
			});

			/**
			 * On a user's (friend or chat user) change, it emits this event. We check for if they begin playing a game, and if they are in the chat.
			 */
			self.steam.on('user', function(user){
				// make sure the user is in the chat room
				if( self.steam.chatRooms[self.settings.steam.chatRoomId] && self.steam.chatRooms[self.settings.steam.chatRoomId][user.friendid] !== undefined ){
					console.log("SUser: " + self.getSteamName(user.friendid));

					// Only output if it isn't the same game and if they started playing a game.
					if( user.gameName.length > 0 && user.gameName !== self.steam.users[user.friendid].gameName ){
						self.send(EProtocol.IRC, user.playerName + " has started playing " + user.gameName + "!");
						self.send(EProtocol.Steam, user.playerName + " has started playing " + user.gameName + "!");
					}
				}
			});

			/**
			 * Pointless function, but the bot needs friends, so.
			 */
			self.steam.on('friend', function(steamId, relationship){
				console.log("SNewFriend: " + self.getSteamName(steamId) + " (" + steamId + ")");
				switch(relationship){
					case steam.EFriendRelationship.PendingInvitee:
						self.steam.addFriend(steamId);
						break;
				};
			});

			/**
			 * If the chat's state changes, this event fires.
			 * 
			 * We make sure we try to rejoin the Steam chat room if we aren't in it here if we are disconnected, kicked or the bot left for some reason.
			 * We also check for if anyone joins, leaves or is kicked or banned from the chat (since those are exclusive).
			 */
			self.steam.on('chatStateChange', function(EChatMemberStateChange, steamId, chatRoomId, actorId){
				console.log("SChatStateChange");

				if( EChatMemberStateChange == steam.EChatMemberStateChange.Kicked ||
					EChatMemberStateChange == steam.EChatMemberStateChange.Disconnected || 
					EChatMemberStateChange == steam.EChatMemberStateChange.Left ){
					self.steam.joinChat(self.settings.steam.chatRoomId); // auto rejoin
				}

				switch( EChatMemberStateChange ){
					case steam.EChatMemberStateChange.Entered:
						self.send(EProtocol.IRC, self.getSteamName(steamId) + " has joined.");
						break;
					case steam.EChatMemberStateChange.Left:
					case steam.EChatMemberStateChange.Disconnected:
						self.send(EProtocol.IRC, self.getSteamName(steamId) + " has left.");
						break;
					case steam.EChatMemberStateChange.Kicked:
						self.send(EProtocol.IRC, self.getSteamName(steamId) + " was kicked by " + self.getSteamName(actorId) + ".");
						break;
					case steam.EChatMemberStateChange.Banned:
						self.send(EProtocol.IRC, self.getSteamName(steamId) + " was banned by " + self.getSteamName(actorId) + ".");
						break;
				}
			});

			/**
			 * Catch-all error listener.
			 */
			self.steam.on('error', function(e){
				console.error('SERROR: ' + e);
			});
		}
	};

	/**
	 * Utility function to make sending to either protocol easier. Allows for a check too.
	 */
	this.send = function(p, msg){
		if( p === EProtocol.Steam ){
			if( self.steam && self.steam.loggedOn ){
				self.steam.sendMessage(self.settings.steam.chatRoomId, msg);
			}
		}
		else if( p === EProtocol.IRC ){
			if( self.irc ){
				self.irc.say(self.settings.irc.output_channel, msg);
			}
		}
	};

	/**
	 * Utility function to get a user's display name
	 */
	this.getSteamName = function(steamId){
		var rv = '';
		if( self.steam.users[steamId] ){
			rv = self.steam.users[steamId].playerName;
		}
		return rv;
	};

	/**
	 * If the bot recognises a command, it will run the function associated with it.
	 */
	// NOTE: change this shit later to use a directory + external files. Lets clean it up
	this.doCommand = function(message){
		if( message.indexOf('/') !== 0 ){
			var cmds = {
				'v': function(){
					var msg = 'Choc version ' + self.info.version + ' by ' + self.info.author + '. Repository: ' + self.info.repo;
					self.send(EProtocol.IRC, msg);
					self.send(EProtocol.Steam, msg);
				},
				'irc': function(){
					self.irc_nicks.sort();
					var msg = "IRC: " + self.irc_nicks.join(', ');

					self.send(EProtocol.IRC, msg);
					self.send(EProtocol.Steam, msg);
				},
				'steam': function(){
					var nicks = [];
					var keys = Object.keys(self.steam.chatRooms[self.settings.steam.chatRoomId]);
					for( var i = 0; i < keys.length; i++ ){
						nicks.push( self.getSteamName(keys[i]) );
					}

					var usersPlaying = [];
					for (var userId in self.steam.chatRooms[self.settings.steam.chatRoomId]) {
						var user = self.steam.users[userId];
						if( user.gameName.length ){
							usersPlaying.push( self.getSteamName(userId) );
						}
					}

					for( var j = 0; j < nicks.length; j++ ){
						if( _.indexOf(usersPlaying, nicks[j]) > -1 ){
							nicks[j] = nicks[j] + '*';
						}
					}

					nicks.sort();

					var msg = "Steam: " + nicks.join(', ');
					self.send(EProtocol.IRC, msg);
					self.send(EProtocol.Steam, msg);
				},
				'playing': function(){
					var games = [];
					for (var userId in self.steam.chatRooms[self.settings.steam.chatRoomId]) {
						var user = self.steam.users[userId];
						if( user.gameName.length ){
							var gameName = user.gameName;
							if( games[gameName] === undefined ){
								games[gameName] = [];
							}
							games[gameName].push( self.getSteamName(userId) );
						}
					}
					games.sort();

					var n = 0;
					for (var game in games){
						self.send(EProtocol.IRC, game + ': ' + games[game].join(', '));
						self.send(EProtocol.Steam, game + ': ' + games[game].join(', '));
						n++;
					}
					if( n === 0 ) {
						var msg = 'No games are being played currently.';
						self.send(EProtocol.IRC, msg);
						self.send(EProtocol.Steam, msg);
					}
				}
			};

			if( message.indexOf(self.settings.commandDelimeter) === 0 ){
				var args = message.split(' ');

				for( var cmd in cmds ){
					if( self.settings.commandDelimeter + cmd === args[0] ){
						cmds[cmd](args);
					}
				}
			}
		}
	};

	/**
	 * Makes the bot play a random game in its library after a random number of minutes.
	 */
	this.doRandomGame = function(){
		var gameId = Math.floor(Math.random() * availableSteamGameIds.length);
		self.steam.gamesPlayed = [availableSteamGameIds[gameId]];

		// Run this function again in 10-60 minutes
		setTimeout(self.doRandomGame, Math.floor(Math.random() * 60 - 10) + 10);
	};
};
